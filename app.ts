class Point {
  x: number;
  y: number;

  constructor(x: number = 0, y: number = 0) {
    this.x = x;
    this.y = y;
  }
}

abstract class Shape {
  protected points: Point[];

  protected constructor(points: Point[]) {
    this.points = [...points];
  }

  protected static getDistance(
    { x: x1, y: y1 }: Point,
    { x: x2, y: y2 }: Point
  ): number {
    return Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);
  }

  public abstract getPerimeter(): number;

  public abstract getSquare(): number;

  public printData(): void {
    console.log(
      `I am a ${this.constructor.prototype.constructor.name}. ` +
      `My perimeter is ${this.getPerimeter().toFixed(2)}. ` +
      `My square is ${this.getSquare().toFixed(2)}.`
    );
  }
}

abstract class Polygon extends Shape {
  protected sides: number[] = [];

  constructor(points: Point[]) {
    super(points);

    for (let i = 0; i < points.length; i++) {
      if (i === points.length - 1) {
        this.sides.push(Shape.getDistance(points[i], points[0]));
      } else {
        this.sides.push(Shape.getDistance(points[i], points[i + 1]));
      }
    }
  }

  public getPerimeter(): number {
    let perimeter: number = 0;

    for (const side of this.sides) {
      perimeter += side;
    }

    return perimeter;
  }

  public abstract getSquare(): number;
}

class Rectangle extends Polygon {
  public getSquare(): number {
    const sortedSides: number[] = [...this.sides]
      .sort((a: number, b: number) => a - b);
    const firstSide = sortedSides[0];
    const secondSide = sortedSides[sortedSides.length - 1];

    return firstSide * secondSide;
  }
}

class Square extends Rectangle {
  public getSquare(): number {
    return this.sides[0] ** 2;
  }
}

class Triangle extends Polygon {
  public getSquare(): number {
    const semiPerimeter: number = this.getPerimeter() / 2;
    let product: number = semiPerimeter;

    for (const side of this.sides) {
      product *= semiPerimeter - side;
    }

    return Math.sqrt(product);
  }
}

class RightTriangle extends Triangle {
  public getSquare(): number {
    const sortedSides: number[] = [...this.sides]
      .sort((a: number, b: number) => a - b);

    return sortedSides[0] * sortedSides[1] / 2;
  }
}

class IsoscelesTriangle extends Triangle {
  public getSquare(): number {
    const sortedSides: number[] = [...this.sides]
      .sort((a: number, b: number) => a - b);
    const lateralSide = sortedSides[0];
    const baseSide = sortedSides[sortedSides.length - 1];

    return baseSide / 2 * Math.sqrt(lateralSide ** 2 - baseSide ** 2 / 4);
  }
}

class EquilateralTriangle extends Triangle {
  public getSquare(): number {
    return this.sides[0] ** 2 * Math.sqrt(3) / 4;
  }
}

class Circle extends Shape {
  radius: number;

  constructor(points: Point[], radius: number) {
    super(points);
    this.radius = radius;
  }

  public getPerimeter(): number {
    return 2 * Math.PI * this.radius;
  }

  public getSquare(): number {
    return Math.PI * this.radius ** 2;
  }
}


const rectangle = new Rectangle([
  new Point(0, 0),
  new Point(4, 0),
  new Point(4, 3),
  new Point(0, 3),
]);
rectangle.printData();

const square = new Square([
  new Point(0, 0),
  new Point(5, 0),
  new Point(5, 5),
  new Point(0, 5),
]);
square.printData();

const arbitraryTriangle = new Triangle([
  new Point(0, 0),
  new Point(4, 0),
  new Point(3, 3)
]);
arbitraryTriangle.printData();

const rightTriangle = new RightTriangle([
  new Point(0, 0),
  new Point(0, 6),
  new Point(4, 0)
]);
rightTriangle.printData();

const isoscelesTriangle = new IsoscelesTriangle([
  new Point(-3, 0),
  new Point(3, 0),
  new Point(0, 4)
]);
isoscelesTriangle.printData();

const equilateralTriangle = new EquilateralTriangle([
  new Point(-3, 0),
  new Point(3, 0),
  new Point(0, 6 * Math.sin(60 * Math.PI / 180))
]);
equilateralTriangle.printData();

const circle = new Circle([new Point()], 3);
circle.printData();
